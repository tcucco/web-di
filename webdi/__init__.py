"""webdi API"""
from .container import Container, ContainerDefinition, _get_key, service

key = _get_key

__all__ = [
    "Container",
    "ContainerDefinition",
    "key",
    "service",
]
