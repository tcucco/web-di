# pylint:disable=missing-docstring,too-few-public-methods
from unittest import IsolatedAsyncioTestCase

from webdi.container import Container, ContainerDefinition, service


class ContainerTest(IsolatedAsyncioTestCase):
    def test_get(self):
        container = get_container()
        srv1 = container.get(Service)
        srv2 = container.get(Service)
        self.assertIsInstance(srv1, Service)
        self.assertEqual(id(srv1), id(srv2))

    async def test_reset(self):
        container = get_container()
        srv1 = container.get(Service)
        await container.reset(Service)
        srv2 = container.get(Service)
        self.assertNotEqual(id(srv1), id(srv2))

    async def test_reset_all(self):
        container = get_container()
        srv1 = container.get(Service)
        await container.reset_all()
        srv2 = container.get(Service)
        self.assertNotEqual(id(srv1), id(srv2))

        self.assertTrue(srv1.d1.cleaned_up)
        self.assertTrue(srv1.d2.cleaned_up)

    async def test_close(self):
        container = get_container()
        srv1 = container.get(Service)

        self.assertFalse(srv1.d1.finalized)
        self.assertFalse(srv1.d2.finalized)

        await container.close()

        self.assertTrue(srv1.d1.finalized)
        self.assertTrue(srv1.d2.finalized)

    def test_has(self):
        container = get_container()
        self.assertFalse(container.has(Service))
        container.get(Service)
        self.assertTrue(container.has(Service))

    def test_add_no_overwrite(self):
        cdef = get_container_definition()
        with self.assertRaises(Exception):
            cdef.add(Dependency1, lambda c: "A String")

    def test_no_cache(self):
        container = get_container()
        srv1 = container.get(Dependency3)
        srv2 = container.get(Dependency3)
        self.assertNotEqual(id(srv1), id(srv2))

    async def test_container_cleanup(self):
        cdef = get_container_definition()
        instance = cdef.get_container()
        self.assertEqual(instance.get(Cleanup), instance.get(Cleanup))

        cleanup_test = instance.get(Cleanup)
        self.assertTrue(cleanup_test.open)

        await cdef.cleanup()

        self.assertFalse(cleanup_test.open)


class Dependency1:
    def __init__(self):
        self.cleaned_up = False
        self.finalized = False

    def finalize(self):
        self.finalized = True

    def cleanup(self):
        self.cleaned_up = True


class Dependency2:
    def __init__(self, dependency_1: Dependency1):
        self.cleaned_up = False
        self.finalized = False
        self.dependency_1 = dependency_1

    async def finalize(self):
        self.finalized = True

    async def cleanup(self):
        self.cleaned_up = True


class Dependency3:
    pass


@service
class Service:
    d1: Dependency1
    d2: Dependency2


class Cleanup:
    def __init__(self):
        self.open = True

    async def close(self):
        self.open = False


def get_container_definition(*, allow_overwrite: bool = False) -> ContainerDefinition:
    cleanup_test = Cleanup()

    cdef = ContainerDefinition(allow_overwrite=allow_overwrite)
    cdef.add(
        Dependency1,
        lambda c: Dependency1(),
        finalize=lambda d: d.finalize(),
        cleanup=lambda d: d.cleanup(),
    )
    cdef.add(
        Dependency2,
        [Dependency1],
        finalize=lambda d: d.finalize(),
        cleanup=lambda d: d.cleanup(),
    )
    cdef.add(Dependency3, lambda c: Dependency3(), cache=False)
    cdef.add(Service)
    cdef.add(Cleanup, lambda c: cleanup_test)

    cdef.add_cleanup(cleanup_test.close)

    return cdef


def get_container() -> Container:
    return get_container_definition().get_container()
